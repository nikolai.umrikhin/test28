Laravel API. Тестовое задание.

1. Get метод API для брендов. Ссылка - .../api/v1/brands

2. Get метод API для моделей. Ссылка - .../api/v1/models/

3. CRUD для автомобилей
    3.1 Получения списка автомобилей. Get метод. Ссылка - .../api/v1/cars/
    3.2 Добавление автомобиля. Post метод. Ссылка - .../api/v1/cars/
        Ключи в теле запроса - 
            brandId 
            modelId 
            year 
            millage 
            color
    3.3 Информация об автомобиле. Get метод. Ссылка - .../api/v1/cars/{id}
    3.4 Редактирования автомобиля. Patch метод. Ссылка - .../api/v1/cars/{id}
        Ключи в теле запроса - 
            brandId 
            modelId 
            year 
            millage 
            color
    3.5 Удаление автомобиля. Delete метод. Ссылка - .../api/v1/cars/{id}
