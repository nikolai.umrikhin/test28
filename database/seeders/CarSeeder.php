<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            ['brand_name' => 'Honda'],
            ['brand_name' => 'Opel'],
            ['brand_name' => 'Fiat'],
        ]);

        DB::table('car_models')->insert([
            ['model_name' => 'Civic', 'brand_id' => '1'],
            ['model_name' => 'Accord', 'brand_id' => '1'],
            ['model_name' => 'Jazz', 'brand_id' => '1'],

            ['model_name' => 'Corsa', 'brand_id' => '2'],
            ['model_name' => 'Mokka', 'brand_id' => '2'],
            ['model_name' => 'Vectra', 'brand_id' => '2'],

            ['model_name' => 'Punto', 'brand_id' => '3'],
            ['model_name' => 'Ducato', 'brand_id' => '3'],
            ['model_name' => 'Tipo', 'brand_id' => '3'],
        ]);

        DB::table('cars')->insert([
            ['brand_id' => '1', 'model_id' => '1', 'year' => '2020', 'millage' => '100500', 'color' => 'red'],
            ['brand_id' => '2', 'model_id' => '1', 'year' => '2010', 'millage' => '200000', 'color' => 'blue'],
            ['brand_id' => '3', 'model_id' => '1', 'year' => '2005', 'millage' => '3230000', 'color' => 'green'],
            ['brand_id' => '1', 'model_id' => '2', 'year' => '1990', 'millage' => '100500', 'color' => 'red'],
            ['brand_id' => '2', 'model_id' => '2', 'year' => '1994', 'millage' => '200000', 'color' => 'blue'],
            ['brand_id' => '3', 'model_id' => '2', 'year' => '1999', 'millage' => '3230000', 'color' => 'green'],

        ]);




    }
}
