<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {


        return [
            'brandId' => ['required'],
            'modelId' => ['required'],
            'year' => ['required','numeric'],
            'millage' => ['required', 'numeric', 'min:0'],
            'color' => ['required', 'max:255'],
        ];
    }
}
