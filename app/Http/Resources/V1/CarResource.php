<?php

namespace App\Http\Resources\V1;

use App\Models\Brand;
use App\Models\CarModel;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $brand = Brand::where('id', $this->brand_id)->first();
        $carModel = CarModel::where('id', $this->model_id)->first();

        return [
            'id' => (string)$this->id,
            'brandId' => (string)$this->brand_id,
            'brandName' => $brand->brand_name,
            'modelId' => (string)$this->model_id,
            'modelName' => $carModel->model_name,
            'year' => (string)$this->year,
            'millage' => (string)$this->millage,
            'color' => $this->color,
        ];

    }
}
