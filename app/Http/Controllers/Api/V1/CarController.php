<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Http\Resources\V1\CarResource;
use App\Http\Resources\V1\CarCollection;
use App\Models\Car;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CarCollection
     */
    public function index()
    {
        return new CarCollection(Car::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreCarRequest $request
     * @return CarResource
     */
    public function store(StoreCarRequest $request)
    {
        $request->validated($request->all());

        $car = Car::create([
            'brand_id' => $request->brandId,
            'model_id' => $request->modelId,
            'year' => $request->year,
            'millage' => $request->millage,
            'color' => $request->color,
        ]);

        return new CarResource($car);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Car $Car
     * @return CarResource
     */
    public function show(Car $Car)
    {
        return new CarResource($Car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateCarRequest $request
     * @param \App\Models\Car $Car
     * @return CarResource
     */
    public function update(UpdateCarRequest $request, Car $Car)
    {
        $Car->update([
            'brand_id' => $request->brandId,
            'model_id' => $request->modelId,
            'year' => $request->year,
            'millage' => $request->millage,
            'color' => $request->color,
        ]);

        return new CarResource($Car);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Car $Car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $Car)
    {
       $Car->delete();

       return response(NULL, 204);

    }


}
