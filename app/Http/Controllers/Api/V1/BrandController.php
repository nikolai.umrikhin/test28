<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Brand;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\BrandCollection;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return BrandCollection
     */
    public function index()
    {
        return new BrandCollection(Brand::all());
    }
}


