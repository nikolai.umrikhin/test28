<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\CarModelCollection;
use App\Models\CarModel;
use App\Http\Controllers\Controller;

class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CarModelCollection
     */
    public function index()
    {
        return new CarModelCollection(CarModel::all());
    }

}
